<?php

namespace Shop\BasketBundle\Controller;

use Shop\BasketBundle\Entity\OrderProduct;
use Shop\BasketBundle\Entity\Orders;
use Shop\BasketBundle\Form\OrdersType;
use Shop\CatalogBundle\Entity\Goods;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BinController
 * @Route("/bin")
 */
class BinController extends Controller
{
    /**
     * @Route("/", name="bin", options = { "expose" = true })
     * @param Request $request
     * @return Response
     */
    public function binAction(Request $request)
    {
        $goods = $this->get('shop_basket.session')->getBasketSession();
        $ids = array();
        $prices = array();
        $totalSum = 0;
        $products = null;

        $orders = new Orders();
        $form = $this->createForm(new OrdersType(), $orders);

        if(count($goods))
        {
            foreach ($goods as $good)
            {
                $totalSum += $good['count'] * $good['price'];
                $prices[] = array('price' => $good['count'] * $good['price'], 'count' => $good['count']);
                $ids[] = $good['id'];
            }
            $products = $this->getDoctrine()->getRepository('CatalogBundle:Goods')->findByArrayId($ids);
        }
        return $this->render('BasketBundle:Bin:bin.html.twig',
            ['products' => $products, 'prices' => $goods, 'totalSum' => $totalSum, 'form' => $form->createView()]);
    }

    /**
     * @Route("/checkout", name="binCheckout", options = { "expose" = true })
     * @param Request $request
     * @return Response
     */
    public function checkoutAction(Request $request)
    {
        $orders = new Orders();
        $form = $this->createForm(new OrdersType(), $orders);
        $goods = $this->get('shop_basket.session')->getBasketSession();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if($form->isValid())
            {
                $em = $this->getDoctrine()->getEntityManager();

                $ids = $this->get('shop_basket.session')->getIdsBasketSession();
                $products = $this->getDoctrine()->getRepository('CatalogBundle:Goods')->findByArrayId($ids);
                $em->persist($orders);

                /** @var Goods $product */
                foreach ($products as $product)
                {
                    $orderProduct = new OrderProduct();
                    $orderProduct->setOrder($orders);
                    $orderProduct->setProductName($product->getName());
                    $orderProduct->setProductPrice($product->getPrice());
                    $orderProduct->setProductQuantity($goods[$product->getId()]['count']);
                    $em->persist($orderProduct);
                }
                $em->flush();
                $this->get('shop_basket.session')->removeBasket();
                return $this->render('@Basket/Bin/success.html.twig');
            }
        }

        return $this->render('@Basket/Bin/checkout.html.twig', ['form' => $form->createView(), 'products' => $goods]);
    }
}
