<?php

namespace Shop\BasketBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class BasketController extends Controller
{
    /**
     * @Route("/basket/add/", requirements={"id" = "\d+"})
     */
    public function addToBasketAction(Request $request)
    {
        $goodsId = $request->get('objectId');

        $objectCount = 1;
        if($request->get('objectCount') !== null){
            $objectCount = (int) $request->get('objectCount');
        }
        $basket = $this->get('shop_basket.session')->getBasketSession();
        $goods = $this->getDoctrine()->getRepository('CatalogBundle:Goods')->find($goodsId);
        if(!$goods)
            throw new EntityNotFoundException();

        if(isset($basket[$goods->getId()]))
        {
            $basket[$goods->getId()] = array(
                'id' => $goods->getId(),
                'count' => (int)$basket[$goods->getId()] * $objectCount,
                'price' => $goods->getPrice()
            );
        }
        else
        {
            $basket[$goods->getId()] = array(
                'id' => $goods->getId(),
                'count' => $objectCount,
                'price' => $goods->getPrice()
            );
        }

        $totalSum = 0;
        $count = 0;
        foreach ($basket as $item) {
            $count += $item['count'];
            $totalSum += $item['price'] * $item['count'];
        }

        $this->get('shop_basket.session')->setBasketSession($basket);

        return new JsonResponse(array('count' => $count, 'totalSum' => $totalSum));
    }

    /**
     * @Route("/removeBasket", name="removeBasket", options = { "expose" = true })
     * @param Request $request
     * @return JsonResponse
     */
    public function removeBasketAction(Request $request)
    {
        $goodsId = $request->get('removeItem');
        $basket = $this->get('shop_basket.session')->getBasketSession();
        $goods = $this->getDoctrine()->getRepository('CatalogBundle:Goods')->find($goodsId);
        $status = 'error';
        if(!$goods)
            $status = 'error';

        if(isset($basket[$goods->getId()]))
        {
            $this->get('shop_basket.session')->removeBasketItemSession($goods->getId());
            $status = 'success';
        }
        return new JsonResponse(array("status" => $status));
    }

    /**
     * @Route("/addItemBasket", name="addItemBasket", options = { "expose" = true })
     * @param Request $request
     * @return JsonResponse
     */
    public function addItemBasketAction(Request $request)
    {
        $count = $request->get('itemCount');
        $goodsId = $request->get('itemId');
        $goods = $this->getDoctrine()->getRepository('CatalogBundle:Goods')->find($goodsId);
        if(!$goods) {
            $status = 'error';
            return new JsonResponse(array("status" => $status));
        }
        $this->get('shop_basket.session')->addBasketItemSession($goods, $count);
        $status = "success";
        $basket = $this->get('shop_basket.session')->getBasketSession();
        $totalSum = 0;
        foreach ($basket as $item) {
            $totalSum += $item['price'] * $item['count'];
        }
        return new JsonResponse(array("status" => $status, 'totalSum' => $totalSum, 'basket' => $basket[$goodsId]));
    }

    public function basketAction()
    {
        $basket = $this->get('shop_basket.session')->getBasketSession();
        $priceSum = 0;
        $count = 0;
        if (is_array($basket) || is_object($basket))
        {
            foreach ($basket as $item) {
                $priceSum += $item['price'] * $item['count'];
                $count += $item['count'];
            }
        }
        return $this->render('@Basket/Basket/basket.html.twig', array('sum' => $priceSum, 'count'=>$count));
    }
}
