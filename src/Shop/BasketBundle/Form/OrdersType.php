<?php

namespace Shop\BasketBundle\Form;

use Gregwar\CaptchaBundle\Type\CaptchaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrdersType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('FIO', 'text', array(
                'label' => 'ФИО',
                'label_attr' => array('class' => 'control-label'),
                'attr' => array('placeholder' => 'ФИО')
            ))
            ->add('phone', 'text', array(
                'label' => 'Телефон',
                'label_attr' => array('class' => 'control-label'),
                'attr' => array('placeholder' => '+996 (XXX) XX-XX-XX')
            ))
            ->add('address', 'textarea', array(
                'label' => 'Аддресс',
                'label_attr' => array('class' => 'control-label'),
                'attr' => array('placeholder' => 'г. Бишкек, ул. Ахунбаева. 99 дом, 99 кв')
            ))
            ->add('submit', 'submit', array(
                'label' => 'Оформить заказ',
                'attr' => array('class' => 'cart-btn')
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Shop\BasketBundle\Entity\Orders'
        ));
    }
}
