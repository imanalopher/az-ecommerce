<?php

namespace Shop\BasketBundle\Session;

use Shop\CatalogBundle\Entity\Goods;
use Symfony\Component\HttpFoundation\Session\Session;

class BasketSession
{
    private $session;

    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    public function setBasketSession($basket)
    {
        $this->session->set('basket', $basket);
    }

    public function getBasketSession()
    {
        $basket = $this->session->get('basket');
        return $basket;
    }

    public function getIdsBasketSession()
    {
        $baskets = $this->session->get('basket');
        $ids = array();
        foreach ($baskets as $basket)
        {
            $ids[] = $basket['id'];
        }
        return $ids;
    }

    public function removeBasketItemSession($id)
    {
        $baskets = $this->session->get('basket');
        if(isset($baskets[$id]))
        {
            unset($baskets[$id]);
            $this->session->set('basket', $baskets);
        }
    }

    /**
     * @param Goods $goods
     */
    public function addBasketItemSession($goods, $count)
    {
        $basket = $this->session->get('basket');

        $basket[$goods->getId()] = array(
            'id' => $goods->getId(),
            'count' => $count,
            'price' => $goods->getPrice()
        );
        $this->setBasketSession($basket);
    }

    public function removeBasket()
    {
        $this->session->clear();
    }
}