<?php

namespace Shop\CatalogBundle\Menu;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Shop\CatalogBundle\Entity\Category;
use Symfony\Component\HttpFoundation\Request;

class BreadcrumbBuilder
{
    private $factory, $em;

    /**
     * @param FactoryInterface $factory
     * @param EntityManager $em
     */
    public function __construct(FactoryInterface $factory, $em)
    {
        $this->em = $em;
        $this->factory = $factory;
    }

    /**
     * @param Request $request
     * @return bool|ItemInterface
     */
    public function createBreadcrumbMenu(Request $request)
    {
        /** @var ItemInterface $menu */
        $menu = $this->factory->createItem('root', ['childrenAttributes' => []]);
        $menu->addChild('Home', array('route' => 'homePage'));

        switch ($request->get('_route'))
        {
            case 'goodsGetInfo':
                $this->getProductBreadcrumb($menu, $request);
                break;
            case 'getCategory':
                $this->getCategoryBreadcrumb($menu, $request);
                break;
            case 'brandInfo':
                $this->getBrandBreadcrumb($menu, $request);
                break;
            case 'binCheckout':
                $this->getCheckoutBreadcrumb($menu, $request);
                break;
            case 'aboutPage':
                $this->getAboutPageBreadcrumb($menu, $request);
                break;
            case 'fos_user_registration_register':
                $this->getRegistrationBreadcrumb($menu, $request);
                break;
            case 'fos_user_security_login':
                $this->getLoginBreadcrumb($menu, $request);
                break;
            case 'uploadProduct':
                $this->getUploadProductBreadcrumb($menu, $request);
                break;
            case 'bin':
                $this->getBinBreadcrumb($menu, $request);
                break;
            case 'returnProductPage':
                $this->getReturnProductPageBreadcrumb($menu, $request);
                break;
            case 'paymentPage':
                $this->getPaymentBreadcrumb($menu, $request);
                break;
            case 'deliveryPage':
                $this->getDeliveryPageBreadcrumb($menu, $request);
                break;
            case 'contactPage':
                $this->getContactPageBreadcrumb($menu, $request);
                break;
            default: break;
        }



        return $menu;
    }

    /**
     * @param ItemInterface $menu
     * @param Request $request
     */
    public function getPaymentBreadcrumb($menu, $request)
    {
        $menu->addChild('Оплата', array('route' => 'paymentPage'));
    }

    /**
     * @param ItemInterface $menu
     * @param Request $request
     */
    public function getContactPageBreadcrumb($menu, $request)
    {
        $menu->addChild('Контакты', array('route' => 'contactPage'));
    }

    /**
     * @param ItemInterface $menu
     * @param Request $request
     */
    public function getDeliveryPageBreadcrumb($menu, $request)
    {
        $menu->addChild('Доставка', array('route' => 'deliveryPage'));
    }

    /**
     * @param ItemInterface $menu
     * @param Request $request
     */
    public function getBinBreadcrumb($menu, $request)
    {
        $menu->addChild('Корзинка', array('route' => 'binCheckout'));
    }

    /**
     * @param ItemInterface $menu
     * @param Request $request
     */
    public function getReturnProductPageBreadcrumb($menu, $request)
    {
        $menu->addChild('Условия возврата товара', array('route' => 'returnProductPage'));
    }

    /**
     * @param ItemInterface $menu
     * @param Request $request
     */
    public function getCheckoutBreadcrumb($menu, $request)
    {
        $menu->addChild('Форма оформления', array('route' => 'binCheckout'));
    }

    /**
     * @param ItemInterface $menu
     * @param Request $request
     */
    public function getUploadProductBreadcrumb($menu, $request)
    {
        $menu->addChild('Добавить товар', array('route' => 'uploadProduct'));
    }

    /**
     * @param ItemInterface $menu
     * @param Request $request
     */
    public function getLoginBreadcrumb($menu, $request)
    {
        $menu->addChild('Логин', array('route' => 'binCheckout'));
    }

    /**
     * @param ItemInterface $menu
     * @param Request $request
     */
    public function getAboutPageBreadcrumb($menu, $request)
    {
        $menu->addChild('О нас', array('route' => 'aboutPage'));
    }
    /**
     * @param ItemInterface $menu
     * @param Request $request
     */
    public function getRegistrationBreadcrumb($menu, $request)
    {
        $menu->addChild('Регистрация', array('route' => 'fos_user_registration_register'));
    }

    /**
     * @param ItemInterface $menu
     * @param Request $request
     */
    public function getBrandBreadcrumb($menu, $request)
    {
        $brands = $this->em->getRepository('CatalogBundle:Manufacture')->find($request->get('id'));
        $menu->addChild($brands->getName(), array('route' => 'brandInfo', 'routeParameters' => array('id'=>$brands->getId())));
    }

    /**
     * @param ItemInterface $menu
     * @param Request $request
     */
    public function getCategoryBreadcrumb($menu, $request)
    {
        $categories = $this->em->getRepository('CatalogBundle:Category')->find($request->get('id'));
        $breadcrumb = array();
        $count = 0;
        $breadcrumb[$count] = array('name' => $categories->getCname(), 'route' => 'getCategory', 'routeParameters' => array('id' => $categories->getId()));

        while($categories->getParent() != null)
        {
            $count++;
            $category = $categories->getParent();
            $breadcrumb[$count] = array('name' => $category->getCname(), 'route' => 'getCategory', 'routeParameters' => array('id' => $category->getId()));
            $categories = $category;
        }
        for ($i = count($breadcrumb) - 1; $i >= 0; $i--)
        {
            $menu->addChild($breadcrumb[$i]['name'], array('route' => $breadcrumb[$i]['route'], 'routeParameters' => $breadcrumb[$i]['routeParameters']));
        }
    }

    /**
     * @param $menu
     * @param Request $request
     */
    public function getProductBreadcrumb($menu, $request)
    {
        $goods = $this->em->getRepository('CatalogBundle:Goods')->find($request->get('id'));
        $categories = $goods->getCategories();
        $breadcrumb = array();
        $count = 0;
        if(isset($categories[0]) || is_array($categories) && is_object($categories)) {

            $breadcrumb[$count] = array('name' => $categories[0]->getCname(), 'route' => 'getCategory', 'routeParameters' => array('id' => $categories[0]->getId()), 'class' => '');
            $categories = $categories[0];
            while ($categories->getParent() != null) {
                $count++;
                if(!$categories->getParent() instanceof Category)
                    break;
                $category = $categories->getParent();
                $breadcrumb[$count] = array('name' => $category->getCname(), 'route' => 'getCategory', 'routeParameters' => array('id' => $category->getId()), 'class' => '');
                $categories = $category;
            }

            for ($i = count($breadcrumb) - 1; $i >= 0; $i--) {
                $menu->addChild($breadcrumb[$i]['name'], array('route' => $breadcrumb[$i]['route'], 'routeParameters' => $breadcrumb[$i]['routeParameters']))->setAttribute('class', '');
            }
        }
        $menu->addChild($goods->getName(), array('route' => 'goodsGetInfo', 'routeParameters' => array('id' => $goods->getId())));
    }
}