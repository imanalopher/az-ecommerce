<?php
namespace Shop\CatalogBundle\Menu;

use Doctrine\ORM\EntityManager;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Shop\CatalogBundle\Entity\Category;
use Shop\CatalogBundle\Entity\Goods;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\RequestStack;

class MenuBuilder extends ContainerAware
{
    /** @var EntityManager $entityManager */
    private $factory, $entityManager;
    protected $container;

    /**
     * @param FactoryInterface $factory
     * @param $entityManager
     * @param ContainerInterface $container
     */
    public function __construct(FactoryInterface $factory, $entityManager, ContainerInterface $container)
    {
        $this->container = $container;
        $this->factory = $factory;
        $this->entityManager = $entityManager;
    }

    public function createMainMenu(RequestStack $requestStack)
    {
        $category = -1;
        $menu = $this->factory->createItem('root', array(
            'childrenAttributes' => array('class' => 'list-unstyled category_menu')
        ));
        $menu->setCurrent($requestStack->getMasterRequest()->get('_route'));

        $menu->addChild('Главная', array('route' => 'homePage'));
        if($requestStack->getMasterRequest()->get('_route') == 'goodsGetInfo')
        {
            $id = $requestStack->getMasterRequest()->get('id');
            /** @var Goods $category */
            $category = $this->entityManager->getRepository('CatalogBundle:Goods')->find($id);
        }
        $this->getCatalogMenu($menu, $category);
        $menu->addChild('Контакт', array('route' => 'aboutPage'));
        return $menu;
    }

    /**
     * @param $menu
     * @param $product
     */
    public function getCatalogMenu($menu, $product)
    {
        $cat = null;
        $categories = $this->entityManager->getRepository('CatalogBundle:Category')->findBy(array('active' => 1, 'parent' => null));

        if($categories) {

            /** @var ItemInterface $menu */
            /** @var Category $category */
            foreach ($categories as $category) {
                if(is_object($product)) {
                    if ($category->getChildren()) {
                        if($this->isCurrent($category, $product))
                        {
                            /** @var ItemInterface $menu */
                            $cat = $menu->addChild($category->getCname(), array('route' => 'getCategory', 'routeParameters' => array('id' => $category->getId())))
                                ->setDisplayChildren(true)->setCurrent(true);
                        }
                        else
                        {
                            $cat = $menu->addChild($category->getCname(), array('route' => 'getCategory', 'routeParameters' => array('id' => $category->getId())))
                                ->setDisplayChildren(true);
                        }
                    }
                    else if ($category->getId() == $product->getCategory()->getId()) {
                        /** @var ItemInterface $menu */
                        $cat = $menu->addChild($category->getCname(), array('route' => 'getCategory', 'routeParameters' => array('id' => $category->getId())))
                            ->setDisplayChildren(true)->setCurrent(true);
                    } else {
                        $cat = $menu->addChild($category->getCname(), array('route' => 'getCategory', 'routeParameters' => array('id' => $category->getId())))
                            ->setDisplayChildren(true);
                    }
                    /** @var Category $child */
                    foreach ($category->getChildren() as $child) {
                        $cat->addChild($child->getCname(), array('route' => 'getCategory', 'routeParameters' => array('id' => $child->getId())))
                            ->setChildrenAttribute('class', 'sub_menu_wrap top_arrow d_xs_none type_2 tr_all_hover clearfix r_corners');
                    }
                }
                else {
                    $cat = $menu->addChild($category->getCname(), array('route' => 'getCategory', 'routeParameters' => array('id' => $category->getId())))
                        ->setDisplayChildren(true);
                    /** @var Category $child */
                    foreach ($category->getChildren() as $child) {
                        $cat->addChild($child->getCname(), array('route' => 'getCategory', 'routeParameters' => array('id' => $child->getId())))
                            ->setChildrenAttribute('class', 'sub_menu_wrap top_arrow d_xs_none type_2 tr_all_hover clearfix r_corners');
                    }
                }
            }
        }
    }

    /**
     * @param Category $category
     * @param Goods $product
     * @return bool
     */
    public function isCurrent($category, $product)
    {
        if($category->getId() == $product->getCategory()->getId())
            return true;
        foreach ($category->getChildren() as $child) {
            if($child->getId() == $product->getCategory()->getId())
            {
                return true;
            }
        }
        return false;
    }
}