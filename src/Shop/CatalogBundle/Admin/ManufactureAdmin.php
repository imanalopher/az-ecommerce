<?php

namespace Shop\CatalogBundle\Admin;

use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;


class ManufactureAdmin extends Admin{

    protected function configureShowField(ShowMapper $showMapper)
    {
        $showMapper
                ->add('id',null,array('label'=>'ID'))
                ->add('name',null,array('label'=>'Заголовок'))
                ->add('active',null,array('label'=>'Активен'));
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('name',null,array('label'=>'Заголовок'))
                ->add('active',null,array('label'=>'Активен','required'=>false))
                ->add('imagePath', 'sonata_type_model_list', array('required' => false, 'label' => 'Галерея'), array('link_parameters' => array('context' => 'default')));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('id',null,array('label'=>'ID'))
                ->addIdentifier('name',null,array('label'=>'Заголовок'))
                ->add('active','boolean',array('editable' => true,'label'=>'Активен'));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('name',null,array('label'=>'Заголовок'));
    }
}