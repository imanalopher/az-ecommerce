<?php

namespace Shop\CatalogBundle\Admin;

use Doctrine\ORM\EntityRepository;
use Shop\CatalogBundle\Entity\Collection;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;


class CategoryAdmin extends Admin{

    protected function configureShowField(ShowMapper $showMapper)
    {
        $showMapper
                ->add('id',null,array('label'=>'ID'))
                ->add('cname',null,array('label'=>'Заголовок'))
                ->add('active',null,array('label'=>'Активен'))
                ->add('parent','entity',array('label'=>'Парент'));
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->add('cname',null,array('label'=>'Заголовок'))
                ->add('active',null,array('label'=>'Активен','required'=>false))
                ->add('parent','entity', array('label'=>'Родитель',
                    'class' => 'CatalogBundle:Category','required'=>false,
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('p');
                    })
                );
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->addIdentifier('id',null,array('label'=>'ID'))
                ->addIdentifier('cname',null,array('label'=>'Заголовок'))
                ->add('active','boolean',array('editable' => true,'label'=>'Активен'))
                ->add('parent','sonata_type_model',array('label'=>'Родитель'))
                ->add('_action', null, array(
                    'label' => 'Управления',
                    'actions' => array(
                        'edit' => array(),
                        'delete' => array()
                    )
                ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('cname',null,array('label'=>'Заголовок'));
    }
}