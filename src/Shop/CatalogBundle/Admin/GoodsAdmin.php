<?php

namespace Shop\CatalogBundle\Admin;

use Doctrine\DBAL\Query\QueryBuilder;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Shop\CatalogBundle\Entity\Goods;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class GoodsAdmin extends AbstractAdmin
{
    protected $translationDomain = 'SonataPageBundle';

    protected $datagridValues = array(
        '_sort_order'=>'DESC',
        '_sort_by'=>'id'
    );

    /**
     * @var Goods $product
     */
    public function prePersist($product)
    {
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $product->setUser($user);
    }

    protected function configureShowField(ShowMapper $showmapper)
    {
        $showmapper
            ->add('id', null, array('label' => 'ID'))
            ->add('name', null, array('label' => 'Название'))
            ->add('shortDescription', null, array('label' => 'Краткое описание'))
            ->add('price', null, array('label' => 'Цена'))
            ->add('oldPrice', null, array('label' => 'Старая Цена'))
            ->add('imagePath', 'sonata_media_type', array('label' => 'Галерея', 'provider' => 'sonata.media.provider.image', 'context' => 'default'))
            ->add('active', null, array('label' => 'Активен'))
            ->add('amount', null, array('label' => 'Количество'))
            ->add('lastUpdate', 'date', array('label' => 'Последнее обновление'))
            ->add('views', null, array('label' => 'Количество просмотров'));

    }

    protected function configureFormFields(FormMapper $formmapper)
    {
        $formmapper
            ->with('General')
            ->add('name', null, array('label' => 'Название'))
            ->add('price', null, array('label' => 'Цена'))
            ->add('oldPrice', null, array('label' => 'Старая Цена'))
            ->add('imagePath', 'sonata_type_model_list', array('required' => false, 'label' => 'Галерея'), array('link_parameters' => array('context' => 'default')))
            ->add('active', null, array('label' => 'Активен'))
            ->add('hot', null, array('label' => 'Новый'))
            ->add('sale', null, array('label' => 'Скидка'))
            ->add('amount', null, array('label' => 'Количество'))
            ->add('code', null, array('label' => 'Штрих код'))
            ->add('lastUpdate', 'date', array('label' => 'Последнее обновление'))
            ->add('shortDescription', null, array('label' => 'Краткое описание'))
            ->add('review', CKEditorType::class,
                array('label' => 'Обзор', 'config' => array('width' => '1300px', 'resize_enabled' => true, 'resize_minHeight' => '745px', 'resize_minWidth' => '700px', 'resize_maxWidth' => '700px'), 'config_name' => 'my_config')
            )
            ->add('categories', 'sonata_type_model', array(
                'label' => 'Категории',
                'multiple' => true,
                'expanded' => true,
                'by_reference' => true,
                'property' => 'name',
            ))
            ->add('brand', 'sonata_type_model', array(
                'label' => 'Бренды',
                'multiple' => false,
                'expanded' => true,
                'by_reference' => true
            ))
            ->end();
    }

    protected function configureListFields(ListMapper $listmapper)
    {
        $listmapper
            ->addIdentifier('name', null, array('label' => 'Название'))
            ->add('price', null, array('editable' => true, 'label' => 'Цена'))
            ->add('oldPrice', null, array('editable' => true, 'label' => 'Ст.Цена'))
            ->add('active', 'boolean', array('editable' => true, 'label' => 'Активен'))
            ->add('code', null, array('label' => 'Штрих код'))
            ->add('category', null, array('label' => 'Категорий', 'template' => 'CatalogBundle:Admin:goods_list_category.html.twig'))
            ->add('image', null, array('label'=>'Фото','template' => 'CatalogBundle:Admin:goods_list_image.html.twig'))
            ->add('imagePath', 'sonata_type_model_list', array('editable' => true, 'label' => 'Галерея'))
            ->add('_action', null, array(
                'actions' => array(
                    'edit' => array(),
                    'delete' => array()
                )
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, array('label' => 'Название'))
            ->add('user', null, array('label' => 'Хозяин'))
            ->add('price', null, array('label' => 'Цена'))
            ->add('code', null, array('label' => 'Штрих код'))
            ->add('active', null, array('label' => 'Активен'))
            ->add('last_update', 'doctrine_orm_date_range', array('label' => 'Последнее обновление'))
            ->add('imagePath', null, array('label' => 'Галерея'), null, array('expanded' => false, 'empty_value' => ""));
    }

}