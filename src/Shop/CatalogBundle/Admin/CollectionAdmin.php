<?php

namespace Shop\CatalogBundle\Admin;

use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;


class CollectionAdmin extends Admin{

    protected function configureShowField(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id', null, array('label'=>'ID'))
            ->add('name', null, array('label'=>'Заголовок'))
            ->add('active', null, array('label'=>'Активен'))
            ->add('image', 'sonata_type_model_list', array('editable' => true, 'label' => 'Галерея'))
            ->add('collectionOrder');
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', null, array('label'=>'Имя'))
            ->add('active', null, array('label'=>'Активен'))
            ->add('image', 'sonata_type_model_list', array('label' => 'Галерея'))
            ->add('category')
            ->add('collectionOrder', null, array('label' => 'Порядок'));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id',null,array('label'=>'ID'))
            ->addIdentifier('name',null,array('label'=>'Заголовок'))
            ->add('image', null, array('label'=>'Фото','template' => 'CatalogBundle:Admin:collection_list_image.html.twig'))
            ->add('active','boolean',array('editable' => true,'label'=>'Активен'));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name',null,array('label'=>'Заголовок'))
            ->add('active',null,array('label'=>'Активен'));
    }
}