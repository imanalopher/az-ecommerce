<?php

namespace Shop\CatalogBundle\Admin;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class OrdersAdmin extends AbstractAdmin
{
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'DESC',
        '_sort_by' => 'id',
    );

    protected function configureShowField(ShowMapper $showmapper)
    {
        $showmapper
            ->add('id', null, array('label' => 'ID'))
            ->add('phone', null, array('label' => 'Название'));

    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('created', null, array('label' => 'Созданно'))
            ->add('status', null, array('label' => 'Статус'))
            ->add('address', 'textarea', array('label' => 'Адрес'))
            ->add('phone', null, array('label' => 'Тел.'));
    }

    protected function configureListFields(ListMapper $listmapper)
    {
        $listmapper
            ->add('id', null, array('label' => 'ID'))
            ->add('created', null, array('label' => 'Созданно'))
            ->add('status', null, array('editable' => true, 'label' => 'Статус'))
            ->add('address', null, array('label' => 'Адрес'))
            ->add('phone', null, array('label' => 'Тел.'))
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'delete' => array()
                )
            ))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('created', null, array('label' => 'orderProducts'))
            ->add('phone', null, array('label' => 'Бренд'));
    }

}