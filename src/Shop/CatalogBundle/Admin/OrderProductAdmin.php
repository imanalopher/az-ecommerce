<?php

namespace Shop\CatalogBundle\Admin;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class OrderProductAdmin extends AbstractAdmin
{

    protected function configureShowField(ShowMapper $showmapper)
    {
        $showmapper
            ->add('id', null, array('label' => 'ID'))
            ->add('phone', null, array('label' => 'Название'));

    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id', null, array('label' => 'E-mail'))
            ->add('productName', null, array('label' => 'Имя продукта'))
            ->add('productPrice', null, array('label' => 'Цена'))
            ->add('productQuantity', null, array('label' => 'Количество'))
            ->add('order', null, array('label' => 'Клиент'));
    }

    protected function configureListFields(ListMapper $listmapper)
    {
        $listmapper
            ->addIdentifier('id', null, array('label' => 'ID'))
            ->add('productName', null, array('label' => 'Имя продукта'))
            ->add('productPrice', null, array('label' => 'Цена'))
            ->add('productQuantity', null, array('label' => 'Количество'))
            ->add('order', null, array('label' => 'Клиент'));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id', null, array('label' => 'Id'))
            ->add('productPrice', null, array('label' => 'Цена'))
            ->add('order', null, array('label' => 'Клиент'));
    }

}