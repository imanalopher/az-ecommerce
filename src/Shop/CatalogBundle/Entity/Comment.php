<?php

namespace Shop\CatalogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Comment
 *
 * @ORM\Table(name="comment")
 * @ORM\Entity(repositoryClass="Shop\CatalogBundle\Repository\CommentRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Comment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Goods
     * @ORM\ManyToOne(targetEntity="Goods", inversedBy="comments")
     * @ORM\JoinColumn(onDelete="cascade")
     */
    private $product;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text")
     * @Assert\Length(
     *     min = 2,
     *     max = 9999,
     *     minMessage = "Поле КОММЕНТАРИИ слишком короткое (Минимум: {{ limit }} симв.).",
     *     maxMessage = "Поле КОММЕНТАРИИ слишком длинное (Макс: {{ limit }} симв.)."
     * )
     */
    private $comment;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetimetz")
     */
    private $created;

    /**
     * @var string
     *
     * @ORM\Column(name="author", type="string", nullable=false)
     */
    private $author;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set comment
     *
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return Goods
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Goods $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt()
    {
        $this->created = new \DateTime();
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param string $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }
}

