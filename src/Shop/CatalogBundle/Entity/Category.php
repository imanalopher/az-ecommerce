<?php

namespace Shop\CatalogBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Shop\CatalogBundle\Entity\Collection;

/**
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="Shop\CatalogBundle\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $cname
     *
     * @ORM\Column(name="cname", type="string", length=100, nullable=false)
     */
    private $cname;

    /**
     * @var ArrayCollection|Goods[]
     * @ORM\ManyToMany(targetEntity="Goods", mappedBy="categories")
     */
    private $goods;

    /**
     * @ORM\OneToMany(targetEntity="Category", mappedBy="parent", cascade={"persist", "remove" }, orphanRemoval=true)
     */
    private $children;

    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    protected $parent;

    /**
     * @var boolean $active
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->goods = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param Category $children
     */
    public function setChildren(Category $children)
    {
        $this->children[] = $children;
    }

    /**
     * @return Category
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param Category $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return mixed
     */
    public function getCname()
    {
        return $this->cname;
    }

    /**
     * @param mixed $cname
     */
    public function setCname($cname)
    {
        $this->cname = $cname;
    }

    public function __toString()
    {
        return $this->cname.'';
    }

    /**
     * @return Collection
     */
    public function getCollection()
    {
        return $this->collection;
    }

    /**
     * @param Collection $collection
     */
    public function setCollection($collection)
    {
        $this->collection = $collection;
    }

    /**
     * @return ArrayCollection|Goods[]
     */
    public function getGoods()
    {
        return $this->goods;
    }

    /**
     * @return Goods $goods
     */
    public function removeGoods(Goods $goods)
    {
        if (false === $this->goods->contains($goods)) {
            return;
        }
        $this->goods->removeElement($goods);
        $goods->removeCategory($this);
    }

    public function addGoods(Goods $goods)
    {
        if (!$this->goods->contains($goods)) {
            $this->goods->add($goods);
            $goods->addCategory($this);
        }
    }

    /**
     * @param Goods $goods
     */
    public function setGoods($goods)
    {
        $this->goods = $goods;
    }

    public function getName()
    {
        $parent = $this->getParent();
        if($parent instanceof Category && $parent->getParent() != null)
            return $this->getParent()->getParent(). ' => '. $this->getParent(). ' => ' .$this->cname;
        else if($parent instanceof Category) {
            return $parent->getCname() . ' => ' . $this->cname;
        }
        else
            return $this->cname;
    }
}