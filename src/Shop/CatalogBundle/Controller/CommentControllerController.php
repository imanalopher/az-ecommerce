<?php

namespace Shop\CatalogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class CommentControllerController extends Controller
{
    /**
     * @Route("/lastComments")
     */
    public function lastCommentsAction()
    {
        $comments = $this->getDoctrine()->getRepository('CatalogBundle:Comment')->findBy([],[], 5);
        return $this->render('CatalogBundle:CommentController:last_comments.html.twig', ['comments' => $comments]);
    }

}
