<?php

namespace Shop\CatalogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Shop\NewsBundle\Entity\News;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/store")
 */
class StoreController extends Controller
{
    /**
     * @Route("/", name="store_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $autoservices = 'Магазины автозапчастей';
        $description = 'Список магазины автозапчастей для вашей машины';
        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->setTitle($autoservices . ' ' . 'в Бишкеке на Detali.kg')
            ->addMeta('name', 'keywords', $autoservices. ' ' . 'в Бишкеке на Detali.kg')
            ->addMeta('property', 'og:title', $autoservices)
            ->addMeta('property', 'og:type', 'product')
            ->addMeta('property', 'og:url', $request->getUri())
            ->addMeta('name', 'description', $description . ' ' . 'в Бишкеке Detali.kg')
        ;
        $stores = $this->getDoctrine()->getRepository('ApplicationSonataUserBundle:User')->findBy(array('isStore' => 1));
        return $this->render('CatalogBundle:Store:index.html.twig', array('stores' => $stores));
    }

    /**
     * Finds and displays a News entity.
     *
     * @Route("/{id}", name="store_show")
     * @Method("GET")
     */
    public function showAction($id)
    {
        $goods = $this->getDoctrine()->getRepository('CatalogBundle:Goods')->findBy(array('user' => $id));
        $user = $this->getDoctrine()->getRepository('ApplicationSonataUserBundle:User')->find($id);
        return $this->render('CatalogBundle:Store:show.html.twig', ['goods' => $goods, 'user' => $user]);
    }
}
