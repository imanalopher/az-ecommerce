<?php
/**
 * Created by PhpStorm.
 * User: imanali
 * Date: 4/16/17
 * Time: 1:22 AM
 */

namespace Shop\CatalogBundle\Controller;

use Doctrine\ORM\EntityManager;
use Shop\CatalogBundle\Entity\Category;
use Shop\CatalogBundle\Entity\Goods;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ExcelAdminController extends Controller {

    /**
     * @Route("/excelForm", name="excelForm")
     * @param Request $request
     * @return Response
     */
    public function excelFromAction(Request $request)
    {

        return $this->render('CatalogBundle:Excel:index.html.twig',array(
            'base_template' => 'CatalogBundle:Admin:standard_layout.html.twig',
            'pages'=>1,
            'admin_pool' => $this->container->get('sonata.admin.pool')));
    }

    /**
     * @Route("/uploadExcel", name="uploadExcel")
     * @param Request $request
     * @return Response
     */
    public function uploadExcelAction(Request $request)
    {
        if($request->isMethod('POST'))
        {
            $directory = $request->server->get('DOCUMENT_ROOT') . '/uploads/excel';
            $uploadedFile = $request->files->get('excel');
            $file = $uploadedFile->move($directory, date('Y-m-d H:i:s').''.$uploadedFile->getClientOriginalName());

            $excel = $this->get('phpexcel')->createPHPExcelObject($file);

            $sheet = $excel->setActiveSheetIndex(0);
            $highestRow = $sheet->getHighestRow();

            if($highestRow > 0) {
                $em = $this->getDoctrine()->getManager();
                for ($row = 2; $row <= $highestRow; $row++) {
                    $goods = new Goods();
                    $data = $sheet->rangeToArray('A' . $row . ':' . 'N' . $row,
                        NULL,
                        TRUE,
                        FALSE)[0];
                    $brand = $this->getDoctrine()->getRepository('CatalogBundle:Manufacture')->findBy(array('name' => $data[8]));

                    if(empty($brand))
                        $brand = null;
                    else
                        $brand = $brand[0];

                    $category = null;
                    $categories = array();

                    if(isset($data[9]))
                    {
                        $category = $this->getCategory(explode('/', $data[9]));
                        if($category) {
                            $categories[] = $category;
                        }
                    }

                    if(isset($data[10]))
                    {
                        $category = $this->getCategory(explode('/', $data[10]));
                        if($category) {
                            $categories[] = $category;
                        }
                    }

                    if(isset($data[11]))
                    {
                        $category = $this->getCategory(explode('/', $data[11]));
                        if($category) {
                            $categories[] = $category;
                        }
                    }

                    if(isset($data[12]))
                    {
                        $category = $this->getCategory(explode('/', $data[12]));
                        if($category) {
                            $categories[] = $category;
                        }
                    }

                    if(isset($data[13]))
                    {
                        $category = $this->getCategory(explode('/', $data[13]));
                        if($category) {
                            $categories[] = $category;
                        }
                    }


                    $goods->setName($data[0]);
                    $goods->setCode($data[1]);
                    $goods->setShortDescription($data[2]);
                    $goods->setReview($data[3]);
                    $goods->setPrice($data[4]);
                    $goods->setOldPrice($data[5]);
                    $goods->setActive($data[6]);
                    $goods->setSale($data[7]);
                    $goods->setBrand($brand);
                    if(count($categories) > 1)
                    {
                        $goods->setCategories($categories);
                    }
                    else {
                        $goods->addCategory($category);
                    }
                    $goods->setAmount(1);
                    $goods->setLastUpdate(new \DateTime());
                    $goods->setUser($this->getUser());

                    $em->persist($goods);

                    $em->flush();
                }
            }
        }

        return $this->render('CatalogBundle:Excel:index.html.twig',array(
            'base_template' => 'CatalogBundle:Admin:standard_layout.html.twig',
            'pages'=>1,
            'admin_pool' => $this->container->get('sonata.admin.pool')));
    }

    public function getCategory(array $brand, $index = 0)
    {
        if(($index + 1) <= count($brand))
            /** @var Category $category */
            $category = $this->getDoctrine()->getRepository('CatalogBundle:Category')->findBy(array('cname' => $brand[$index]));
        else
            return null;

        if ($category)
        {
            $category = $category[0];
            $categories = $this->getDoctrine()->getRepository('CatalogBundle:Category')->findBy(array('parent' => $category->getId()));
            if(!$categories) {
                return null;
            }
            foreach($categories as $category)
            {
                if($category->getCname() == $brand[1])
                {
                    $childs = $this->getDoctrine()->getRepository('CatalogBundle:Category')->findBy(array('parent' => $category->getId()));
                    if(isset($brand[2]) && count($brand) >= $index)
                    {
                        if(!$childs)
                            return null;
                        foreach ($childs as $child)
                        {
                            if ($child->getCname() == $brand[2])
                            {
                                return $child;
                            }
                        }
                    }
                    else
                        return $category;
                }
            }
        }
        return null;
    }

    /**
     * @Route("/updateExcelProduct", name="updateExcelProduct")
     * @param Request $request
     * @return Response
     */
    public function updateExcelProductAction(Request $request)
    {
        if($request->isMethod('POST'))
        {
            $directory = $request->server->get('DOCUMENT_ROOT') . '/uploads/excel';
            $uploadedFile = $request->files->get('excel');
            $file = $uploadedFile->move($directory, date('Y-m-d H:i:s').''.$uploadedFile->getClientOriginalName());

            $excel = $this->get('phpexcel')->createPHPExcelObject($file);

            $sheet = $excel->setActiveSheetIndex(0);
            $highestRow = $sheet->getHighestRow();

            if($highestRow > 0) {
                $em = $this->getDoctrine()->getManager();
                for ($row = 2; $row <= $highestRow; $row++) {
                    $data = $sheet->rangeToArray('A' . $row . ':' . 'N' . $row,
                        NULL,
                        TRUE,
                        FALSE)[0];
                    $goods = $this->getDoctrine()->getRepository('CatalogBundle:Goods')->find($data[0]);

                    if($goods) {
                        $goods->setPrice($data[1]);
                        $goods->setLastUpdate(new \DateTime());
                        $em->persist($goods);
                        $em->flush();
                    }
                }
            }
        }

        return $this->render('CatalogBundle:Excel:updateExcelProduct.html.twig',array(
            'base_template' => 'CatalogBundle:Admin:standard_layout.html.twig',
            'pages'=>1,
            'admin_pool' => $this->container->get('sonata.admin.pool')));
    }
}