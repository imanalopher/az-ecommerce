<?php

namespace Shop\CatalogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/autoservice")
 */
class AutoserviceController extends Controller
{
    /**
     * @Route("/", name="service_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $autoservices = 'Автосервисы, Автомагазины';
        $description = 'Лучшие автосервисы и автомагазины для вашей машины';
        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->setTitle($autoservices . ' ' . 'в Бишкеке на Detali.kg')
            ->addMeta('name', 'keywords', $autoservices. ' ' . 'в Бишкеке на Detali.kg')
            ->addMeta('property', 'og:title', $autoservices)
            ->addMeta('property', 'og:type', 'product')
            ->addMeta('property', 'og:url', $request->getUri())
            ->addMeta('name', 'description', $description . ' ' . 'в Бишкеке Detali.kg')
        ;
        $stores = $this->getDoctrine()->getRepository('ApplicationSonataUserBundle:User')->findBy(array('isService' => 1));
        return $this->render('CatalogBundle:Autoservice:index.html.twig', array('stores' => $stores));
    }
}
