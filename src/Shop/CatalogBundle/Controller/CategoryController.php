<?php

namespace Shop\CatalogBundle\Controller;

use Shop\CatalogBundle\Entity\Category;
use Shop\CatalogBundle\Entity\Goods;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CategoryController extends Controller
{
    /**
     * @property array category_ids
     */
    private $category_ids = array();

    /**
     * @Route("/", name="homePage")
     */
    public function indexAction()
    {
        $category = $this->getDoctrine()->getRepository('CatalogBundle:Category')->findBy(array('parent' => null));

        return $this->render('@Catalog/Category/index.html.twig', array('categories' => $category));
    }

    /**
     * @Route("/catalog/{id}", options={"expose" = true }, name="getCategory", requirements={"id" = "\d+"})
     * @param Category $category
     * @param Request $request
     * @return Response
     */
    public function getCategoryAction(Category $category, Request $request)
    {
        if(!$category)
            throw new NotFoundHttpException();

        $parents = $this->getDoctrine()->getRepository('CatalogBundle:Category')->findBy(array('parent' => $category));

        $this->recursiveCategory($category);

        $brand = null;
        $price = array(0, 5000);

        if($request->query->get('brand') !== null && $request->query->get('price') !== null)
        {
            $price = explode(',', substr_replace(substr_replace($request->query->get('price'), '', 0, 1), '', strlen(substr_replace($request->query->get('price'), '', 0, 1)) - 1, 1));
            $brand = $request->query->get('brand');
            $brand = substr_replace($brand, '', 0, 1);
            $brand = substr_replace($brand, '', strlen($brand) -1, 1);
            $brand = explode(',',  $brand);
            $goods = $this->getDoctrine()->getRepository('CatalogBundle:Goods')->findPriceAndBrand($this->category_ids, $price, $brand);
        }
        else {
            $goods = $this->getDoctrine()->getRepository('CatalogBundle:Goods')->findByCategory($this->category_ids);
        }

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $goods, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            12/*limit per page*/
        );
        $seoPage = $this->container->get('sonata.seo.page');
        $seoPage
            ->setTitle($seoPage->getTitle()." - ".$category->getCname())
            ->addMeta('property', 'og:title', $category->getCname())
            ->addMeta('property', 'og:type', 'website')
            ->addMeta('property', 'og:url', $request->getUri())
        ;
        $minGoods = $this->getDoctrine()->getRepository('CatalogBundle:Goods')->findBy(['active' => true],['reviews' => 'ASC'], 1);

        $brands = $this->getDoctrine()->getRepository('CatalogBundle:Manufacture')->findBy(array('active' => true));

        return $this->render('CatalogBundle:Category:getCategory.html.twig',
            ['parents' => $parents, 'price' => $price, 'category' => $category, 'brands' => $brands, 'brnd' => $brand, 'goods' => $pagination, 'minGoods' => $minGoods]
        );
    }

    public function recursiveCategory(Category $category){
        $this->category_ids[] += $category->getId();
        /** @var Category $child */
        foreach($category->getChildren() as $child){
            $this->category_ids[] += $child->getId();
            if($child->getChildren()){
                $this->getChildrenId($child);
            }
        }
    }

    public function getChildrenId(Category $category){
        /** @var Category $child */
        foreach($category->getChildren() as $child) {
            $this->category_ids[] += $child->getId();
        }
    }

    public function getAllCategoriesAction()
    {
        $category = $this->getDoctrine()->getRepository('CatalogBundle:Category')->findBy(array('parent' => null));

        return $this->render('CatalogBundle:Category:getAllCategories.html.twig', array('categories' => $category));
    }

    public function categoryMenuAction()
    {
        $categories = $this->getDoctrine()->getRepository('CatalogBundle:Category')->findBy(array('parent' => null, 'active' => 1));
        return $this->render('CatalogBundle:Category:categoryMenu.html.twig', ['categories' => $categories]);
    }
}
