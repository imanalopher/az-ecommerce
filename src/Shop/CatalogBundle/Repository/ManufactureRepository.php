<?php

namespace Shop\CatalogBundle\Repository;
use Doctrine\ORM\EntityRepository;

class ManufactureRepository extends EntityRepository
{
    public function findByArrayId($arr)
    {
        $qb = $this->createQueryBuilder('m');
        $qb->where($qb->expr()->in('m.id', $arr));

        return $qb->getQuery()->getResult();
    }
}
