<?php

namespace Shop\CatalogBundle\Form;

use Gregwar\CaptchaBundle\Type\CaptchaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class CommentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('comment', 'textarea', array(
                'attr' => array('class' => 'form-control', 'placeholder' => 'Комментарий', 'rows' => 5)
            ))
            ->add('author', TextType::class, array(
                'label' => 'Автор',
                'attr' => array('class' => 'form-control', 'placeholder' => 'Имя')
            ))
            ->add('submit', 'submit', array(
                'label' => 'Оставить комментарий',
                'attr' => array('class' => 'subscribe-btn')
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Shop\CatalogBundle\Entity\Comment'
        ));
    }
}
