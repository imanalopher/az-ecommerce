<?php

namespace Shop\CatalogBundle\Form;

use Application\Sonata\MediaBundle\Entity\Media;
use Doctrine\ORM\EntityRepository;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GoodsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                'label' => 'Название товара',
                'attr' => array('class' => 'form-control', 'placeholder' => 'Название товара')
            ))
            ->add('price', 'money', array(
                'label' => 'Цена',
                'attr' => array('class' => 'form-control', 'placeholder' => 'Цена')
            ))
            ->add('characteristic', 'text', array(
                'label' => 'Характеристика',
                'attr' => array('class' => 'form-control', 'placeholder' => 'Характеристика')
            ))
            ->add('review', CKEditorType::class, array(
                'label' => 'Обзор',
                'config' => array(
                    'width' => '800px',
                    'resize_enabled' => true,
                    'resize_minHeight' => '745px',
                    'resize_minWidth' => '70px',
                    'resize_maxWidth' => '200px'
                ),
                'config_name' => 'my_config')
            )
            ->add('imagePath', 'file', array(
                'label' => 'Фото',
                'attr' => array('class' => 'form-control')
            ))
            ->add('amount', 'integer', array(
                'label' => 'Количество',
                'attr' => array('class' => 'form-control', 'placeholder' => 'Количество')
            ))
            ->add('category','entity', array('label'=>'Меню',
                    'class' => 'CatalogBundle:Category',
                    'required' => false,
                    'expanded' => true,
                    'by_reference' => true,
                    'multiple' => false,
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('p')
                            ->where('p.active = true');
                    })
            )
            ->add('submit', 'submit', array(
                'label' => 'Добавить товар',
                'attr' => array('class' => 'btn btn-primary')
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Shop\CatalogBundle\Entity\Goods'
        ));
    }

}
