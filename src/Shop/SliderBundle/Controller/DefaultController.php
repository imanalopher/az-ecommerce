<?php

namespace Shop\SliderBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="sliderIndexPage")
     */
    public function indexAction()
    {
        $sliders = $this->getDoctrine()->getRepository('ShopSliderBundle:Slider')->findBy(array('active' => 1), array('sliderOrder' => 'ASC'));
        return $this->render('ShopSliderBundle:Default:index.html.twig', array("sliders" => $sliders));
    }
}
